/*****************************************
 *
 * JOB-SYSTEM STRUCTS
 * 
 * source to simulate an in-game character ~
 * ~ job system.
 * 
 * Todo: 
 *  + add methods as needed [growth rates!!!]
 * 
 *****************************************/



#ifndef __JOBS_H
#define __JOBS_H

#include <string>
#include <vector>
#include "../headers/item.h"
#include "../headers/stats.h"

struct stctJOB {
	
	stctSTATS objSTATSMAX;								// struct object for upper limit on stats per job
	stctSTATS objSTATSMIN;								// struct object for lower limit on stats per job
	stctSTATS objJOBGROWTHRATES;						// struct object containing data for base growth rates on job
	std:: string job_name;								// job's name
	bool can_fly;										// checks to see if winged unit can pass through obstructions
	bool can_swim;										// checks to see if ground unit can traverse water uninhibitied
	int movement;										// number of spaces unit can move per turn
	int primary_weapon;									// primary weapon type
	int secondary_weapon;								// secondary weapon type
	
	/*
	 * reads input from csv so that jobs can be filled
	 */
	void read_from_csv(int pDESIRED_ID);	
	
	/*
	 * defaults things
	 */
	stctJOB();
	
	/*
	 * gives explicit values to object
	 */
	stctJOB(std:: string pNAME, bool pFLY, bool pSWIM, int pMOVE, int pWEPONE, int pWEPTWO);	
};

#endif
