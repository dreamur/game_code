/*****************************************
 *
 * LEVEL STRUCTS
 * 
 * source to simulate levels in a turn-based-
 * tactics game. this is the big one.
 * 
 * Todo: 
 *  + everything
 * 
 *****************************************/



#ifndef __LEVELS_H
#define __LEVELS_H

#include "../headers/character.h"

struct stctLEVEL {
	
	std:: vector<stctCHARACTER> player_roster;						// player's army
	std:: vector<stctCHARACTER> enemies;							// list of AI controlled characters
	std:: vector< std:: vector< int > > grid;						// the level map
	
	/*
	 * displays player options
	 * 	- attack
	 *  - move
	 *  - trade
	 *  - quit game
	 */
	void display_pause_menu();
	
	/*
	 * calls calc damage for attacker
	 * 	- subtracts that number from defender's health
	 */
	void battle (stctCHARACTER pATTACKER, stctCHARACTER pDEFENDER);
	
	/*
	 * ends level sim if conditions are met [player's leader dies or player defeats all enemies or extrenuous case]
	 */
	bool check_end_condition();
	
	/*
	 * runs move_character & calculate_damage for battle [where necessary] on each of the enemy characters
	 */ 
	void simulate_AI_turn();
	
};

#endif

