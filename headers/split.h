/****************************************
 *
 * SPLIT FN
 * 
 * modified version of fn learned in class.
 * 	used to grab input from CSVs
 * 
 ****************************************/
 
 

#ifndef __SPLIT_H
#define __SPLIT_H
#include <vector>
#include <string>
   
/*
 * This is a list of characters that will be ignored
 * 	while reading in from the CSVs
 */   
const std::string delimiters = "\n\t~!@#%^&*()_+={}|\\{};:.<>,?\"";

/*
 * breaks lines into 'readable chunks' it stores in a vector
 * 	then returns that vector of those chunks
 */
std::vector<std::string> split(const std::string & s, const std::string& delim = delimiters);

#endif
