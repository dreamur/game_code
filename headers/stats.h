/*****************************************
 *
 * Stats STRUCTS
 * 
 * source to simulate an character stats.
 * lightweight. 
 * 
 * Todo: 
 *  + add methods as needed
 * 
 *****************************************/



#ifndef __STATS_H
#define __STATS_H

#include <iostream>

struct stctSTATS {
		
	int ATK;
	int DEF;
	int MAG;
	int SPD;
	int RES;
	int SKL;
	int LUK;
	int HP;
	
	/*
	 * dumps contents to ostream
	 */
	void output_values();
	
	/*
	 * defaults values to zero
	 */
	stctSTATS();
	
	/*
	 * generates stats from given values
	 */
	stctSTATS (int pATK, int pDEF, int pMAG, int pSPD, int pRES, int pSKL, int pLUK, int pHP);
};

#endif
