/*****************************************
 *
 * CHARACTER STRUCT
 * 
 * source to simulate an in-game character ~
 * ~ stat system.
 * 
 * Todo: 
 *  + add/ modify methods as needed
 * 	+ implement methods to manipulate growth rates
 * 
 *****************************************/



#ifndef __CHARACTER_H
#define __CHARACTER_H

#include "../headers/inventory.h"
#include "../headers/jobs.h"
#include <random>

struct stctCHARACTER {
	
	//stctSTATS objSTATS;							NOTE:	 Base stats of the character will be read in from csv
	//														 and total stats will be dynamically calculated as needed 
	//														 i.e. Base stats are never stored in program
	stctSTATS objLVLUPSTATS;						// Stat boosts obtained through level up
	stctSTATS objCHARGROWTHRATES;
	stctJOB objJOB; 
	stctINVENTORY objINVENTORY;
	stctITEM objEQUIPPEDITEM; 
	std:: string name;
	int level;
	bool ai_controlled;
	double weapon_knowledge [7];
	
	/*
	 * creates a character object with given parameters
	 */
	stctCHARACTER (int pJOBID, stctINVENTORY pOBJINVENTORY, std:: string pNAME, int pLVL, bool NPC, double pWEP_KNOWLDGE[]);

	/*
	 * calls get damage_boost [from item header] and adds to attack [# before subtracting from def/res]
	 */
	int calculate_damage ();
	
	/*
	 * calls add_item for character's inventory
	 */
	void give_item(stctITEM pITEM);
	
	/*
	 * calls output_items in character's inventory
	 */
	void display_inventory();
	
	/*
	 * selects weapon to be 'equipped'
	 */
	void equip_weapon(stctITEM pITEM);
	
	/*
	 * calls remove_item in inventory
	 */
	void remove_item(stctITEM pITEM);
	
	/*
	 * increases level and random number of character stats
     */	
	void level_up();
	
	/*
	 * selects a different job for character
	 */
	void change_job(stctJOB pJOB);
	
	/*
	 * display vars for character struct & 
	 * calls output fn for stats struct
	 */
	void display_stats();
	 
	/*
	 * set health to max after battle
	 */
	void restore_health(); 
};

#endif
