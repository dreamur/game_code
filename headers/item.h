/****************************************
 *
 * ITEM STRUCT
 * 
 * source to simulate an in-game inventory ~
 * ~ system.
 * 
 * Todo: 
 *	+ this now compiles again;
 * 	+ the == operator does not work for fns
 * 		+ will have to overload it separately for fns
 * 		+ if a full comparison is desired
 * 		+ PROJECTED FIX - remove lambdas from struct
 * 			+ add modifiers to CSV (i.e. column for each stat increase)
 * 			+ add fn(s) in code to manipulate those modifiers
 * 
 ****************************************/
 
 

#ifndef __ITEM_H
#define __ITEM_H

#include <fstream>
#include <iostream>
#include "../headers/split.h"

struct stctITEM {
		
	std:: string name;								// text for item identification for player
	int type;										// 0 - consumable; 1 - sword; 2 - spear; 3 - axe; 4 - tome; 5 - staff; 6 - shapeshifting stone; 7 - bow
	int damage_boost;								// number to be added to MAG or ATK stat during attack phase
	bool physical_damage;							// damage_boost combines w/ ATK; otherwise combines w/ MAG
	int accuracy;									// simplified for now
	int crit_chance;								// 0 - 100; 0 is impossible; 100 is certain
	int uses;
	int base_value;
	
	/* 
	 * used to compare values of items
	 * Only true if every member is identical for both objects
	 */ 
	bool operator == (stctITEM pItem);
	
	/*
	 * makes a new stctITEM object
	 */
	stctITEM(std:: string pNAME, int pTYPE, int pDAMBOOST, bool pISPHYSICAL, int pACCURACY, int pCRIT, int pUSES, int pVALUE);
	
	/*
	 * returns type of item
	 */
	int return_item_type();
	
	/*
	 * defaults all members to zero [or equivalent] so that character objects may be created
	 * this was an issue w/ character.h explicit constructor not giving values to
	 * the equipped_item struct; look for alternative methods for equipping?
	 */
	stctITEM();
	
	/*
	 * reads values from a csv to create a desired item object
	 */ 	
	stctITEM read_from_csv(int pDESIRED_ID);
};

#endif
