/****************************************
 *
 * INVENTORY STRUCT
 * 
 * source to simulate an in-game item ~
 * ~ attributes.
 * 
 * Todo: 
 *  + this now compiles again;
 *
 ****************************************/



#ifndef __INVENTORY_H
#define __INVENTORY_H

#include "../headers/item.h"
#include <algorithm>

struct stctINVENTORY {
	
	int max_size;											// 5 for characters; 50 for players
	std::vector <stctITEM> vec_inventory;					// where all items are stored
	stctITEM NULL_ITEM { " ", -1, 0, false, 0, 0, 0, 0 };	// A placeholder item
		
	/*
	 * creates a stctITEM object by calling the read_from_csv fn in the item struct
	 */
	void make_item (int pDESIRED_ID);
	
	/*
	 * adds a stctITEM object to the inventory vector
	 */
	void add_item (stctITEM pITEM);
	
	/*
	 * removes first matching element from the vec_inventory vector
	 */
	void remove_item (stctITEM pITEM); 
	
	/*
	 * default constructor [for character structs]
	 */
	stctINVENTORY();
	
	/*
	 * explicit constructor [no stctITEM]
	 * SIZE	- maximum size flag to determine what type/size of inventory to create
	 */
	stctINVENTORY (int pSIZE);
	
	/*
	 * explicit constructor [w/ stctITEM]
	 * SIZE	- maximum size flag to determine what type/size of inventory to create
	 * stctITEM - stctITEM object from make_item fn to be pushed into vector
	 */
	stctINVENTORY (int pSIZE, stctITEM pITEM);
	
	/*
	 * sort ascending by name [lexicographical]
	 */
	void sort_by_name ();
	 
	/*
	 * sort descending by durability [largest durability first]
	 */
	void sort_by_durability ();
	
	/*
	 * sort descending by attack bonus [largest attack bonus first]
	 */
	void sort_by_attack ();
	
	/*
	 * sort descending by accuracy bonus [largest accuracy bonus first]
	 */
	void sort_by_accuracy ();
	
	/*
	 * dumps contents of stctITEM objects in vector to ostream
	 */ 
	void output_items ();
	
	/*
	 * removes stctITEM from one inventory and adds same stctITEM to another inventory
	 */
	 void trade_item (stctINVENTORY& pOBJECT, stctITEM pITEM); 
	 
	/*
	 * calls the read_from_csv fn in the item scope and adds a new item in the first
	 * 	available spot in inventory
	 */  
	 stctITEM read_from_csv (int pDESIRED_ID);
	
};

#endif
