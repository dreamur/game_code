#include "../headers/stats.h"

void stctSTATS:: output_values() {
	
	std:: cout << "ATK: " << ATK
			   << "\tDEF: " << DEF
			   << "\nMAG: " << MAG
			   << "\tSPD: " << SPD
			   << "\nRES: " << RES
			   << "\tSKL: " << SKL
			   << "\nLUK: " << LUK
			   << '\n';	
}

stctSTATS:: stctSTATS() {
	
	ATK = DEF = MAG = SPD = RES = SKL = LUK = HP = 0;
}

stctSTATS:: stctSTATS (int pATK, int pDEF, int pMAG, int pSPD, int pRES, int pSKL, int pLUK, int pHP) {
	
	ATK = pATK;
	DEF = pDEF;
	MAG = pMAG;
	SPD = pSPD;
	RES = pRES;
	SKL = pSKL;
	LUK = pLUK;
	HP = pHP;
}
