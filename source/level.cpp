#include "../headers/level.h"

void stctLEVEL:: display_pause_menu() {
	
	std:: cout << "Attack\n"
			   << "Move\n"
			   << "Trade\n"
			   << "Quit game\n";

	char choice;
	std:: cin >> choice;
	
	switch (choice) {
		
		case 'a':
			// call battle fn
			std:: cout << " ";
			break;
		case 'm':
			// call character's move fn
			std:: cout << " ";
			break;
		case 't':
			// call trade item fns
			std:: cout << " ";
			break;
		case 'q':
			return;
			
		default:
			std:: cout << "Error message goes here\n";
			break;		
	}
}

void stctLEVEL:: battle (stctCHARACTER pATTACKER, stctCHARACTER pDEFENDER) {
	
	// HP MOVED TO STATS STRUCT - REWORK THIS FN
/*	if (pATTACKER.objEQUIPPEDITEM.type == 5 || pATTACKER.objEQUIPPEDITEM.type == 4) 
		pDEFENDER.health -= abs(pATTACKER.calculate_damage() - pDEFENDER.objSTATS.RES);
	else
		pDEFENDER.health -= abs(pATTACKER.calculate_damage() - pDEFENDER.objSTATS.DEF);*/
}

bool stctLEVEL:: check_end_condition() {
	
	if (player_roster.empty()) {
		std:: cout << "Game over...\n";
		return true;
	}
	
	if (enemies.empty()) {
		std:: cout << "Victory!\n";
		return true;		
	}
	return false;
}

void stctLEVEL:: simulate_AI_turn() {
	
	
	
}
