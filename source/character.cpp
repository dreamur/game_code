#include "../headers/character.h"


stctCHARACTER:: stctCHARACTER (int pJOBID, stctINVENTORY pOBJINVENTORY, std:: string pNAME, int pLVL, bool NPC, double pWEP_KNOWLDGE[]) {
	
	objJOB.read_from_csv(pJOBID);
	name = pNAME;
	level = pLVL;
	ai_controlled = NPC;
	for (int i = 0; i < 7; ++i) { pWEP_KNOWLDGE[i] = 0.0; }
}

int stctCHARACTER:: calculate_damage () {
/*	
	int item_type = objEQUIPPEDITEM.type;
	double weapon_proficiency;
	
	if (item_type == objJOB.primary_weapon) { weapon_proficiency = objJOB.wep_proficiency_one; }
	else if (item_type == objJOB.secondary_weapon) { weapon_proficiency = objJOB.wep_proficiency_two; }
	
	// physical damage
	if (objEQUIPPEDITEM.physical_damage == true) {
		return ( objEQUIPPEDITEM.damage_boost + ((objSTATS.ATK + level + objSTATS.SKL) * weapon_proficiency) );
	}	
	
	// magic damage
	else if (objEQUIPPEDITEM.physical_damage == false) {
		return ( objEQUIPPEDITEM.damage_boost + ((objSTATS.MAG + level + objSTATS.SKL) * weapon_proficiency) );
	}
	REWORK THIS STUFF 
*/
}
	
void stctCHARACTER:: give_item(stctITEM pITEM) {

	objINVENTORY.add_item(pITEM);
}
	
void stctCHARACTER:: display_inventory() {

	objINVENTORY.output_items();
}
	
void stctCHARACTER:: equip_weapon(stctITEM pITEM) {
	
	if (!objINVENTORY.vec_inventory.empty()) { // makes certain there are elements in container
		
		if (pITEM.type != 0) { // cannot equip a consummable
			
			// if the item is not found, it won't be equipped
			if (std:: find (objINVENTORY.vec_inventory.begin(), objINVENTORY.vec_inventory.end(), pITEM) != objINVENTORY.vec_inventory.end())
				{ objEQUIPPEDITEM = pITEM; }
			else { std:: cout << "The character does not have that item.\n"; }
		}	 
		else { std:: cout << "This item cannot be equipped.\n"; }
	}
	else { std:: cout << "The character does not have any items.\n"; }
}
	
void stctCHARACTER:: remove_item(stctITEM pITEM) {
	
	if (!objINVENTORY.vec_inventory.empty()) { // makes certain there are elements in container
			
			// if the item is not found, it won't be removed
			if (std:: find (objINVENTORY.vec_inventory.begin(), objINVENTORY.vec_inventory.end(), pITEM) != objINVENTORY.vec_inventory.end())
				{ objINVENTORY.remove_item (pITEM); }
			else { std:: cout << "The character does not have that item.\n"; }
		}	 
	else { std:: cout << "The character does not have any items to remove.\n"; }
}
	
void stctCHARACTER:: level_up() {
	
	std:: default_random_engine generator;
	std:: uniform_int_distribution <int> distribution (1, 100);
	
	int total_growth_rates [8]; 
	total_growth_rates [0] = objCHARGROWTHRATES.ATK + objJOB.objJOBGROWTHRATES.ATK;
	total_growth_rates [1] = objCHARGROWTHRATES.DEF + objJOB.objJOBGROWTHRATES.DEF;
	total_growth_rates [2] = objCHARGROWTHRATES.MAG + objJOB.objJOBGROWTHRATES.MAG;
	total_growth_rates [3] = objCHARGROWTHRATES.SPD + objJOB.objJOBGROWTHRATES.SPD;
	total_growth_rates [4] = objCHARGROWTHRATES.RES + objJOB.objJOBGROWTHRATES.RES;
	total_growth_rates [5] = objCHARGROWTHRATES.SKL + objJOB.objJOBGROWTHRATES.SKL;
	total_growth_rates [6] = objCHARGROWTHRATES.LUK + objJOB.objJOBGROWTHRATES.LUK;
	total_growth_rates [7] = objCHARGROWTHRATES.HP  + objJOB.objJOBGROWTHRATES.HP;
	
	for (int i = 0; i < 8; ++i) {
		
		int random_number = distribution (generator);
		
		switch (i) {
			
			case 0:
				if (total_growth_rates [0] > 99) { total_growth_rates [0] -= 100, objLVLUPSTATS.ATK += 1; }
				if (total_growth_rates [0] > random_number) { objLVLUPSTATS.ATK += 1; }
				break;
			case 1:
				if (total_growth_rates [1] > 99) { total_growth_rates [1] -= 100, objLVLUPSTATS.DEF += 1; }
				if (total_growth_rates [0] > random_number) { objLVLUPSTATS.DEF += 1; }
				break;
			case 2:
				if (total_growth_rates [2] > 99) { total_growth_rates [2] -= 100, objLVLUPSTATS.MAG += 1; }
				if (total_growth_rates [0] > random_number) { objLVLUPSTATS.MAG += 1; }
				break;
			case 3:
				if (total_growth_rates [3] > 99) { total_growth_rates [3] -= 100, objLVLUPSTATS.SPD += 1; }
				if (total_growth_rates [0] > random_number) { objLVLUPSTATS.SPD += 1; }
				break;
			case 4:
				if (total_growth_rates [4] > 99) { total_growth_rates [4] -= 100, objLVLUPSTATS.RES += 1; }
				if (total_growth_rates [0] > random_number) { objLVLUPSTATS.RES += 1; }
				break;
			case 5:
				if (total_growth_rates [5] > 99) { total_growth_rates [5] -= 100, objLVLUPSTATS.SKL += 1; }
				if (total_growth_rates [0] > random_number) { objLVLUPSTATS.SKL += 1; }
				break;
			case 6:
				if (total_growth_rates [6] > 99) { total_growth_rates [6] -= 100, objLVLUPSTATS.LUK += 1; }
				if (total_growth_rates [0] > random_number) { objLVLUPSTATS.LUK += 1; }
				break;
			case 7:
				if (total_growth_rates [7] > 99) { total_growth_rates [7] -= 100, objLVLUPSTATS.HP += 1; }
				if (total_growth_rates [0] > random_number) { objLVLUPSTATS.HP += 1; }
				break;
		}
	}	
}	

void stctCHARACTER:: change_job(stctJOB pJOB) {
	
	objJOB = pJOB;
}

void stctCHARACTER:: display_stats() {
	
/*	std:: cout << name << "\n\n"
			   << "Level: " << level
			   << "HP: " << objSTATS.HP << '\n';
	objSTATS.output_values();		
	
	REWORK THIS FN 
*/   
}

void stctCHARACTER:: restore_health() {
	
}
