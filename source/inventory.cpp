#include "../headers/inventory.h"

void stctINVENTORY:: make_item (int pDESIRED_ID) {
	
	if (vec_inventory.size() >= max_size) { return; }	// i.e. the inventory is full
	else { read_from_csv(pDESIRED_ID); }
}

void stctINVENTORY:: add_item (stctITEM pITEM) {
	
	if (vec_inventory.size() >= max_size) { std:: cout << "Inventory is full.\n"; }
	else { vec_inventory.emplace_back(pITEM); }
}
	
void stctINVENTORY:: remove_item(stctITEM pITEM) {
	
	if (vec_inventory.size() < 1) { std:: cout << "Inventory is empty.\n"; }
	for (auto it = vec_inventory.begin(); it != vec_inventory.end(); ++it) {
 		if ((*it) == pITEM) { 
			(*it) = NULL_ITEM;						
			break; 
		}											// adding a break will prevent the code from removing duplicates
 		if (it == vec_inventory.end()) { std:: cout << "The item could not be found in the inventory.\n"; }
 	}
}

stctINVENTORY:: stctINVENTORY() {
	
	max_size = 5;
}

stctINVENTORY:: stctINVENTORY(int pSIZE) {
	
	max_size = (pSIZE == 50 ? 50 : 5);
}

stctINVENTORY:: stctINVENTORY(int pSIZE, stctITEM pITEM) {
	
	max_size = (pSIZE == 50 ? 50 : 5);
	vec_inventory.emplace_back(pITEM);
}

void stctINVENTORY:: sort_by_name () {
	
	std:: sort(vec_inventory.begin(), vec_inventory.end(), [](stctITEM a, stctITEM b) { return a.name < b.name; } );
}

void stctINVENTORY:: sort_by_durability () {

	std:: sort(vec_inventory.begin(), vec_inventory.end(), [](stctITEM a, stctITEM b) { return a.uses > b.uses; } );
}

void stctINVENTORY:: sort_by_attack () {

	std:: sort(vec_inventory.begin(), vec_inventory.end(), [](stctITEM a, stctITEM b) { return a.damage_boost > b.damage_boost; } );
}

void stctINVENTORY:: sort_by_accuracy () {

	std:: sort(vec_inventory.begin(), vec_inventory.end(), [](stctITEM a, stctITEM b) { return a.accuracy > b.accuracy; } );
}

void stctINVENTORY:: output_items () {
	
	if (vec_inventory.empty()) { std:: cout << "Empty Inventory\n"; }
	else {
			
		std:: cout << "NAME\tATK\tACC\tDUR\tVAL\n\n";
		for (auto itm : vec_inventory) {
			std:: cout << itm.name << '\t' << itm.damage_boost << '\t' << itm.accuracy << '\t' << itm.uses << '\t' << itm.base_value << '\n';
		}
	}
}

void stctINVENTORY:: trade_item (stctINVENTORY& pOBJECT, stctITEM pITEM) {
	
	this->remove_item(pITEM);
	pOBJECT.add_item(pITEM);	
}

stctITEM stctINVENTORY:: read_from_csv (int pDESIRED_ID) {
	
	if (vec_inventory.size() >= max_size) { std:: cout << "The item cannot be created, your inventory is full.\n"; }
	else {
		for (auto item : vec_inventory) {	
			
			if (item == NULL_ITEM) { 
				item.read_from_csv(pDESIRED_ID);
				break;
			}
		}
	}
}
