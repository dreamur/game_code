#include "../headers/jobs.h"

void stctJOB:: read_from_csv(int pDESIRED_ID){
	
	stctITEM objITEM;
	std:: ifstream ifstr ("./csvs/jobs_list.csv", std:: ifstream:: in);
	std:: string line;
	std:: vector<std:: string> current_JOB;
	
	while ( std:: getline(ifstr, line) ) {
		
		current_JOB = split (line);
		if ( stoi(current_JOB[0]) == pDESIRED_ID) { break; }
	}	
	if ( stoi(current_JOB[0]) != pDESIRED_ID) {
		
		std:: cout << "No matching item ID.\n";
	}
	else {
		objSTATSMAX.ATK = std:: stoi (current_JOB[7]);
		objSTATSMAX.DEF = std:: stoi (current_JOB[8]);
		objSTATSMAX.MAG = std:: stoi (current_JOB[9]);
		objSTATSMAX.SPD = std:: stoi (current_JOB[10]);
		objSTATSMAX.RES = std:: stoi (current_JOB[11]);
		objSTATSMAX.SKL = std:: stoi (current_JOB[12]);
		objSTATSMAX.LUK = std:: stoi (current_JOB[13]);
		objSTATSMAX.HP	= std:: stoi (current_JOB[21]);
		objSTATSMIN.ATK = std:: stoi (current_JOB[14]);
		objSTATSMIN.DEF = std:: stoi (current_JOB[15]);
		objSTATSMIN.MAG = std:: stoi (current_JOB[16]);
		objSTATSMIN.SPD = std:: stoi (current_JOB[17]);
		objSTATSMIN.RES = std:: stoi (current_JOB[18]);
		objSTATSMIN.SKL = std:: stoi (current_JOB[19]);
		objSTATSMIN.LUK = std:: stoi (current_JOB[20]);
		objSTATSMIN.HP	= std:: stoi (current_JOB[22]);
		objJOBGROWTHRATES.ATK = std:: stoi(current_JOB[23]);
		objJOBGROWTHRATES.DEF = std:: stoi(current_JOB[24]);
		objJOBGROWTHRATES.MAG = std:: stoi(current_JOB[25]);
		objJOBGROWTHRATES.SPD = std:: stoi(current_JOB[26]);
		objJOBGROWTHRATES.RES = std:: stoi(current_JOB[27]);
		objJOBGROWTHRATES.SKL = std:: stoi(current_JOB[28]);
		objJOBGROWTHRATES.LUK = std:: stoi(current_JOB[29]);
		objJOBGROWTHRATES.HP  = std:: stoi(current_JOB[30]);
		job_name = current_JOB[1];
		can_fly = std:: stoi (current_JOB[4]);
		can_swim = std:: stoi (current_JOB[5]);	
		movement = std:: stoi (current_JOB[6]);		
		primary_weapon = std:: stoi (current_JOB[2]);					
		secondary_weapon = std:: stoi (current_JOB[3]);
	}
}

stctJOB:: stctJOB() {
	
	job_name = " ";
	can_fly = false;
	can_swim = false;
	movement = 1;
	primary_weapon = 1;
	secondary_weapon = 2;
}

stctJOB:: stctJOB(std:: string pNAME, bool pFLY, bool pSWIM, int pMOVE, int pWEPONE, int pWEPTWO)	{
	
	job_name = pNAME;
	can_fly = pFLY;
	can_swim = pSWIM;
	movement = pMOVE;
	primary_weapon = pWEPONE;
	secondary_weapon = pWEPTWO;
}
