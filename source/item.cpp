#include "../headers/item.h"

stctITEM:: stctITEM(std:: string pNAME, int pTYPE, int pDAMBOOST, bool pISPHYSICAL, int pACCURACY, int pCRIT, int pUSES, int pVALUE) {

	name = pNAME;					
	type = pTYPE;								
	damage_boost = pDAMBOOST;		
	physical_damage = pISPHYSICAL;				
	accuracy = pACCURACY;		
	crit_chance = pCRIT;						
	uses = pUSES;
	base_value = pVALUE;	
}

bool stctITEM:: operator == (stctITEM pITEM) {

	return (name == pITEM.name && type == pITEM.type &&
			damage_boost == pITEM.damage_boost &&
			physical_damage == pITEM.physical_damage &&
			accuracy == pITEM.accuracy &&
			crit_chance == pITEM.crit_chance && 
			uses == pITEM.uses && base_value == pITEM.base_value
			);
}

int stctITEM:: return_item_type() {

	return (this->type < 1 ? 0 : type);
}

stctITEM:: stctITEM() {
	
	name = " ";
	type = 0;
	damage_boost = 0;
	physical_damage = false;
	accuracy = crit_chance = uses = base_value = 0;
}

stctITEM stctITEM:: read_from_csv(int pDESIRED_ID) {
	
	stctITEM objITEM;
	std:: ifstream ifstr ("../csvs/items_list.csv", std:: ifstream:: in);
	std:: string line;
	std:: vector<std:: string> current_ITEM;
	
	while ( std:: getline(ifstr, line) ) {
		
		current_ITEM = split (line);
		if ( stoi(current_ITEM[0]) == pDESIRED_ID) { break; }
	}	
	if ( stoi(current_ITEM[0]) != pDESIRED_ID) {
		
		std:: cout << "No matching item ID.\n";
	} 
	else {
		name = current_ITEM[1];								
		type = std:: stoi (current_ITEM[2]);									
		damage_boost = std:: stoi (current_ITEM[3]);								
		physical_damage = std:: stoi (current_ITEM[4]);							
		accuracy = std:: stoi (current_ITEM[5]);									
		crit_chance = std:: stoi (current_ITEM[6]);								
		uses = std:: stoi (current_ITEM[7]);
		base_value = std:: stoi (current_ITEM[8]);
	}
}
